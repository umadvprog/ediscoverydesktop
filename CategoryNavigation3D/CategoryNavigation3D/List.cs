﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace CategoryNavigation3D
{
    public class List : SQLFacade
    {
        //protected abstract DataSet GetDtaSet(string strSQL);
        public List(String project, bool byProjectName)
            : base(project, byProjectName) // trying to connect SQLFacade class
        {
            //                                                    cata  ,         list of documents in this cata
            Dictionary<string, List<string>> hashCata = new Dictionary<string, List<string>>();

            //                                              doc    ,          list of cata in this document?
            Dictionary<string, List<string>> hashDoc = new Dictionary<string, List<string>>();

            DataSet ds = GetDtaSet("Data Source=(localdb)"); //DataSet ds = GetDtaSet(SQL);
            DataTable t = ds.Tables[0];

            string colName = "CategoryString";
            List<string> catalist = (from r in t.AsEnumerable() select r[colName]).Distinct().OfType<string>().ToList();
            List<string> doclist  = (from r in t.AsEnumerable() select r[colName]).Distinct().OfType<string>().ToList();

            Dictionary<string, List<string>> Hashcata = new Dictionary<string, List<string>>();
            Dictionary<string, List<string>> Hashdoc = new Dictionary<string, List<string>>();
            foreach (string cata in catalist)
            {
              //add this catagory to the hash
                Hashcata.Add(cata, new List<string>());
            }

            foreach (string doc in doclist)
            {
              //add this catagory to the hash
                Hashdoc.Add(doc, new List<string>());
            }

            //Owen's code
            createLists(Hashcata, Hashdoc);             //calls a function to generate the category and document lists from the tables
        }

        //public string strSQL { get; set; }

        //Owen's code
        //generate the category and document lists from the tables
        public void createLists(Dictionary<string, List<string>> c, Dictionary<string, List<string>> d)
        {
            foreach (string cata in c.Keys)
            {
                DesktopApp.setBaseCat(new Category(cata));
            }

            foreach (string cata in c.Keys)
            {
                DesktopApp.setBaseCat(new Category(cata));
            }

        }

    }
}