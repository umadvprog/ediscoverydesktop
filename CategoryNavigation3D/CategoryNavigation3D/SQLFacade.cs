﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CategoryNavigation3D
{
    /// <summary>
    /// Serves as an interface between the application and the database
    /// </summary>
    public class SQLFacade
    {
        #region Fields

        private string connectionString;
        private bool databaseCreated = false;
        private SqlConnection connection;

        public SqlConnection GetSQLConnection()
        {
            return connection;
        }

        public SqlDataReader ConnectQueryCloseDB()
        {
            connection = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = "SELECT Z.[Category ID], [Category String], [E-item Count], Y.[E-item ID], [Document ID], W.[File ID], [File Name], [File Type], [Email ID] FROM (SELECT TOP 100 * FROM CategoryData.Category) Z, CategoryData.[Category-E-item] Y, EitemData.[E-item] X, (SELECT CONCAT(A.[E-item ID], B.[E-item ID]) as [E-item ID], A.[File ID], A.[File Name], A.[File Type], [Email ID], [Document ID] FROM (SELECT [E-item ID], A.[File ID], [File Name], [File Type], [Email ID] FROM ProjectData.FileTable A LEFT JOIN EitemData.[Email Item] B ON A.[File ID]=B.[File ID] ) AS A LEFT JOIN (SELECT [E-item ID], A.[File ID], [File Name], [File Type], [Document ID] FROM ProjectData.FileTable A LEFT JOIN EitemData.Document C ON A.[File ID]=C.[File ID]) AS B ON A.[File ID] = B.[File ID]) W WHERE Z.[Category ID] = Y.[Category ID] AND Y.[E-item ID] = X.[E-item ID] AND X.[E-item ID] = W.[E-item ID]";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = connection;

            connection.Open();

            reader = cmd.ExecuteReader();

            return reader;
        }

        #endregion

        #region Methods

        #region Constructors


        /// <summary>
        /// Constructor, uses an existing database
        /// </summary>
        /// <param name="project">The name of the database or the path to the database .mdf file</param>
        /// <param name="byProjectName">Indicates whether project is the database name or path</param>
        public SQLFacade(String project, bool byProjectName)
        {
            connectionString = CreateBasicConnection();

            if (byProjectName)
            {
                databaseCreated = (CheckDatabaseExists(project) == 1);
                if (databaseCreated)
                {
                    AttachProject(project);
                }
            }
            else
            {
                //TODO: Add check for eDiscovery database here
                databaseCreated = true;
                AttachMDFFile(project);
            }
        }


        /// <summary>
        /// Creates the string for connecting to a localdb instance
        /// </summary>
        /// <returns>The connection string</returns>
        private static String CreateBasicConnection()
        {
            string instancename = "v11.0";
            return ("Data Source=(LocalDB)\\" + instancename + ";" +
                "Integrated Security=SSPI;");
        }


        /// <summary>
        /// Attaches the database to connect to to a connection string
        /// </summary>
        /// <param name="ProjectName">The name of the project and database</param>
        private void AttachProject(String ProjectName)
        {
            connectionString += "Initial Catalog=" + ProjectName;
        }

        /// <summary>
        /// Attaches the database file to connect to to a connection string
        /// </summary>
        /// <param name="mdfFile">The path to the mdf file</param>
        private void AttachMDFFile(String mdfFile)
        {
            connectionString += "AttachDbFileName=" + mdfFile;
        }

        //  private void Init_Data(int CurPage)
        //  {            
        //      string SQL = string.Format(" SELECT CategoryData.Category.CategoryID, CategoryString, E-itemCount, CategoryData.Category-E-item.E-itemID, DocumentID, EitemDta.Document.FileID, FileNmae, FileType, EmailID FROM "
        //                   + " CategoryData.Category, CategoryData.Category-E-item, EitemData.E-item, EitemData.Document, ProjectData.FileTable, EitemData.EmailItem WHERE"
        //                   + " CategoryData.Category.CategoryID = ' + CategoryData.Category-E-item.CategoryID + ' and CategoryData.Category-E-item.E-itemID = 'EitemData.E-item.E-itemID'"
        //                   + " and EitemData.E-item.E-itemID = ' EitemData.Document.E-itemID ' and EitemData.Document.FileID = ' ProjectData.FileTable.FileID '"
        //                   + " and ProjectData.FileTable.FileID = ' EitemData.EmailItem.FileID '");
        //  }

        #endregion


        #region Database Checks



        /*
      * /// <summary>
        /// Get SQL connection
        /// </summary>
        /// <param name="cString"></param>
        /// <returns></returns>
        public static SqlConnection GetConnection(String cString)
        {
            return new SqlConnection(cString);
        }
      **/

        /*
         * /// <summary>
           /// read data from database
           /// </summary>
           /// <returns></returns>
           public static SqlDataReader GetCataData()
           {
               SqlDataReader reader= null;
               string cString = CreateBasicConnection();
               string SQL = string.Format(" SELECT CategoryData.Category.CategoryID, CategoryString, E-itemCount, CategoryData.Category-E-item.E-itemID, DocumentID "
                                               + " EitemDta.Document.FileID, FileName, FileType, EmailID FROM "
                                               + " CategoryData.Category, CategoryData.Category-E-item, EitemData.E-item, EitemData.Document, ProjectData.FileTable, EitemData.EmailItem WHERE "
                                               + " CategoryData.Category.CategoryID = ' + CategoryData.Category-E-item.CategoryID + ' and CategoryData.Category-E-item.E-itemID = 'EitemData.E-item.E-itemID' "
                                               + " and EitemData.E-item.E-itemID = ' EitemData.Document.E-itemID ' and EitemData.Document.FileID = ' ProjectData.FileTable.FileID' "
                                               + " and ProjectData.FileTable.FileID = ' EitemData.EmailItem.FileID' ");

               using (SqlConnection connection = new SqlConnection(cString))
               {
                   // Attempt to connect to the localdb instance
                   bool connected = TryDatabaseConnection(connection);

                   if (connected)
                   {
                       try
                       {
                           // Check whether the query returns the database
                           using (SqlCommand command = new SqlCommand(SQL, connection))
                           {
                               reader = command.ExecuteReader();
                           }
                       }
                       catch (SqlException ex)
                       {
                           Console.WriteLine(ex.Message);
                       }
                   }
               }
               return reader;
           }
         * */



        /// <summary>
        /// Connected slqreader, but this function may be not used
        /// </summary>
        /// <param name="strSQL"></param>
        /// <returns></returns>
        public static SqlDataReader GetReader(String strSQL)
        {
            SqlConnection connection = new SqlConnection(CreateBasicConnection());
            try
            {
                bool connected = TryDatabaseConnection(connection);
                SqlCommand myConnection = new SqlCommand(strSQL, connection);
                SqlDataReader reader = myConnection.ExecuteReader(CommandBehavior.CloseConnection);

                return reader;
            }
            catch
            {
                connection.Close();
                throw;
            }
        }

        public static SqlDataReader GetReader(string strSQL, params SqlParameter[] values)
        {
            SqlConnection connection = new SqlConnection(CreateBasicConnection());
            try
            {
                connection.Open();
                SqlCommand myConnection = new SqlCommand(strSQL, connection);
                myConnection.Parameters.AddRange(values);
                SqlDataReader reader = myConnection.ExecuteReader(CommandBehavior.CloseConnection);

                return reader;
            }
            catch
            {
                connection.Close();
                throw;
            }
        }


        /// <summary>
        /// Incoming SQL statement that returns a DataSet types of data collection of tables
        /// </summary>
        /// <param name="strSQL">SQL statement string</param>
        /// <returns>Dataset</returns>
        public static DataSet GetDtaSet(string strSQL) //(String strSQL)
        {
            // Gets a DataSet for a given SQL select statement. 
            using (SqlConnection connection = new SqlConnection(CreateBasicConnection()))
            {
                bool connected = TryDatabaseConnection(connection);
                //connection.Open();

                string SQL = string.Format(" SELECT CategoryData.Category.CategoryID, CategoryString, E-itemCount, CategoryData.Category-E-item.E-itemID, DocumentID "
                                            + " EitemDta.Document.FileID, FileName, FileType, EmailID FROM "
                                            + " CategoryData.Category, CategoryData.Category-E-item, EitemData.E-item, EitemData.Document, ProjectData.FileTable, EitemData.EmailItem WHERE "
                                            + " CategoryData.Category.CategoryID = ' + CategoryData.Category-E-item.CategoryID + ' and CategoryData.Category-E-item.E-itemID = 'EitemData.E-item.E-itemID' "
                                            + " and EitemData.E-item.E-itemID = ' EitemData.Document.E-itemID ' and EitemData.Document.FileID = ' ProjectData.FileTable.FileID' "
                                            + " and ProjectData.FileTable.FileID = ' EitemData.EmailItem.FileID' ");


                SqlCommand myConnection = new SqlCommand(SQL, connection);
                SqlDataAdapter myCommand = new SqlDataAdapter(myConnection);
                DataSet ds = new DataSet();   // Define the cache in data 

                try
                {
                    myCommand.Fill(ds);       // Filling data                    
                }
                catch (Exception ex)
                {
                    throw new System.ApplicationException(ex.Source + " : " + ex.Message + " : " + SQL);
                    //throw new Exception(ex.ToString());
                }
                finally
                {
                    connection.Close();     // Close connection
                    connection.Dispose();   // Release connection
                    myCommand.Dispose();    // Release resource
                }
                return ds;        // Return dataset;
            }
        }

        public static DataSet GetDataSet(string StrSql, SqlParameter[] Parms, CommandType cmdtype)
        {
            DataSet dt = new DataSet(); ;
            using (SqlConnection connection = new SqlConnection(CreateBasicConnection()))
            {
                SqlDataAdapter da = new SqlDataAdapter(StrSql, connection);
                da.SelectCommand.CommandType = cmdtype;
                if (Parms != null)
                {
                    da.SelectCommand.Parameters.AddRange(Parms);
                }
                da.Fill(dt);
            }
            return dt;
        }

        /// <summary>
        /// Incoming SQL statement returns a DataTable
        /// </summary>
        /// <param name="strSQL"></param>
        /// <param name="cmdtype"></param>
        /// <returns></returns>
        public static DataTable GetDataTable(string strSQL)
        {
            // Gets a DataSet for a given SQL select statement. 
            using (SqlConnection connection = new SqlConnection(CreateBasicConnection()))
            {
                bool connected = TryDatabaseConnection(connection);
                //connection.Open();

                string SQL = string.Format(" SELECT CategoryData.Category.CategoryID, CategoryString, E-itemCount, CategoryData.Category-E-item.E-itemID, DocumentID "
                                            + " EitemDta.Document.FileID, FileName, FileType, EmailID FROM "
                                            + " CategoryData.Category, CategoryData.Category-E-item, EitemData.E-item, EitemData.Document, ProjectData.FileTable, EitemData.EmailItem WHERE "
                                            + " CategoryData.Category.CategoryID = ' + CategoryData.Category-E-item.CategoryID + ' and CategoryData.Category-E-item.E-itemID = 'EitemData.E-item.E-itemID' "
                                            + " and EitemData.E-item.E-itemID = ' EitemData.Document.E-itemID ' and EitemData.Document.FileID = ' ProjectData.FileTable.FileID' "
                                            + " and ProjectData.FileTable.FileID = ' EitemData.EmailItem.FileID' ");


                SqlCommand myConnection = new SqlCommand(SQL, connection);
                SqlDataAdapter myCommand = new SqlDataAdapter(myConnection);
                DataSet ds = new DataSet();   // Define the cache in data 

                try
                {
                    myCommand.Fill(ds);       // Filling data
                    if (ds.Tables[0].Rows.Count < 1)
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ApplicationException(ex.Source + " : " + ex.Message + " : " + SQL);
                    //throw new Exception(ex.ToString());
                }
                finally
                {
                    connection.Close();     // Close connection
                    connection.Dispose();   // Release connection
                    myCommand.Dispose();    // Release resource
                }
                return ds.Tables[0];        // Return dataset;
            }
        }


        /// <summary>
        /// Execute Sql
        /// </summary>
        /// <param name="strSQL"></param>
        /// <returns></returns>
        public static bool ExecTSQL(string[] strSQL)
        {

            using (SqlConnection connection = new SqlConnection(CreateBasicConnection()))
            {
                SqlTransaction trans = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {
                    connection.Open();
                    for (int i = 0; i < strSQL.Length; i++)
                    {
                        if (strSQL[i] == "" || strSQL[i] == null)
                        {
                            continue;
                        }

                        SqlCommand sqlCmd = connection.CreateCommand();
                        sqlCmd.Transaction = trans;
                        sqlCmd.CommandText = strSQL[i];
                        sqlCmd.ExecuteNonQuery();
                    }
                    trans.Commit();
                    return true;
                }
                catch (Exception)
                {
                    trans.Rollback();
                    return false;
                }
            }
        }


        /// <summary>
        /// Checks if a database exists
        /// </summary>
        /// <param name="ProjectName">The name of the database to check</param>
        /// <returns>Whether the database exists or not</returns>
        public static int CheckDatabaseExists(String ProjectName)
        {
            string cString = CreateBasicConnection();

            ProjectName = FormatApostrophe(ProjectName);
            string commandString = "if db_id('" + ProjectName + "') is not null " +
                    "SELECT name FROM master.sys.databases WHERE name = '" + ProjectName + "'";


            using (SqlConnection connection = new SqlConnection(cString))
            {
                // Attempt to connect to the localdb instance
                bool connected = TryDatabaseConnection(connection);

                if (connected)
                {
                    try
                    {
                        // Check whether the query returns the database
                        using (SqlCommand command = new SqlCommand(commandString, connection))
                        {
                            SqlDataReader reader = command.ExecuteReader();
                            bool databaseExists = reader.Read();
                            object[] currentRecord = new object[1];

                            while (reader.Read())
                            {
                                reader.GetSqlValues(currentRecord);
                                string name = currentRecord[0].ToString();
                            }
                            reader.Close();

                            if (databaseExists)
                            {
                                return 1;
                            }
                            else
                            {
                                return 0;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }

            // An exception occurred during connection or execution
            return 2;
        }


        /// <summary>
        /// Attempts to connect to a database or localdb instance
        /// </summary>
        /// <param name="connection">The SqlConnection to attempt to connect</param>
        /// <returns>Whether connection was successful or not</returns>
        private static bool TryDatabaseConnection(SqlConnection connection)
        {
            try
            {
                if (connection == null) // Estimate connection if that is null
                {
                    connection = new SqlConnection();
                }
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Formats a string so that apostrophes don't cause problems in SQL queries
        /// </summary>
        /// <param name="input">The string to modify</param>
        /// <returns>The modified string</returns>
        private static String FormatApostrophe(String input)
        {
            return input.Replace("'", "''");
        }


        /// <summary>
        /// Formats a string so that brackets don't cause problems in SQL queries
        /// </summary>
        /// <param name="input">The string to modify</param>
        /// <returns>The modified string</returns>
        private String FormatBrackets(String input)
        {
            return input.Replace("]", "]]");
        }


        /// <summary>
        /// Returns whether the database this SQLFacade connects to exists or not
        /// </summary>
        public bool CheckDatabaseCreated()
        {
            return databaseCreated;
        }

        #endregion

        #endregion
    }
}