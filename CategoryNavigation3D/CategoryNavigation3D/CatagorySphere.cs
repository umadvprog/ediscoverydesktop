﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace CategoryNavigation3D
{
    public class CatagorySphere
    {
        private float radius = 20;
        private byte segments = 30;
        private byte rings = 30;
        private double CatagoryX, CatagoryY, CatagoryZ;
        private float[] catagoryColor = { 0, 0, 0, 0 };

        public CatagorySphere(double x, double y, double z, string color)
        {
            CatagoryX = x;
            CatagoryY = y;
            CatagoryZ = z;

            setColor(color);
        }

        public void drawCatagory(double x, double y, double z)
        {
            var vertices = Sphere.CalculateVertices(radius, radius, segments, rings);
            var elements = Sphere.CalculateElements(radius, radius, segments, rings);

            GL.PushMatrix();

            GL.Material(MaterialFace.Front, MaterialParameter.Ambient, catagoryColor);
            GL.ColorMaterial(MaterialFace.FrontAndBack, ColorMaterialParameter.Ambient);

            GL.Translate(CatagoryX + x, CatagoryY + y, CatagoryZ + z);

            GL.Begin(PrimitiveType.Triangles);
            foreach (var element in elements)
            {
                var vertex = vertices[element];
                GL.TexCoord2(vertex.TexCoord);
                GL.Normal3(vertex.Normal);
                GL.Vertex3(vertex.Position);
            }
            GL.End();

            GL.PopMatrix();
        }

        private void setColor(string color)
        {
            switch (color)
            {
                case "red":
                    catagoryColor = new float[] { 1, 0, 0, 1 };
                    break;
                case "blue":
                    catagoryColor = new float[] { 0, 0, 1, 1 };
                    break;
            }
        }

        public float getRadius() { return radius; }
        public void setRadius(float newRadius) { radius = newRadius; }
    }
}
