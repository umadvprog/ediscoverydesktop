﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryNavigation3D
{
    class DesktopApp
    {
        static List<Category> baseCat = new List<Category>();           //an array that holds all the base categories 
        static List<Category> selCat = new List<Category>();            // An array that holds all the selected categories to be sorted into the ven.
        static List<Category> venCat = new List<Category>();           // an array that holds all the venCategories that have been established.
        static List<Document> baseDoc = new List<Document>();           //an array that holds all the documents


        // takes the selected category and combines it with every category after it in the list.
        // As well as recursively running itself with the newly created categories to create every combination
        // **
        // Category a: The first category to be combined with the laterones
        // int i: marker starting with the locationo f the first category AFTER Category a
        // **
        public static void ComplexCategories(Category a, int i)
        {
            //loop through each remaining category.
            for (int j = i; j < selCat.Count(); j++)
            {
                Category temp = merge(a, selCat[j]);            //creates a new category created from teh merge function
                if (temp != null && !checkCopy(venCat, temp))
                {
                    venCat.Add(temp);                               //adds it to the ven diagram list
                    ComplexCategories(temp, j + 1);                 //runs the ComplexCategories function with the new category
                }
            }
        }



        // Takes the items from within each Category and compares them to each other to create a list of only the ones
        // that are in both categories.  It does this by scanning through their sorted ID's and matching the combinations.
        // **
        // Category a: The first category to be called on
        // Category b: the second category to be called on.
        // **
        private static Category merge(Category a, Category b)
        {
            //will not create a category if a and b are the same category
            if (a == b)
                return null;

            //will not create a category if category b is already part of category a
            if (a.Contains(b))
                return null;

            Category temp = new Category(a, b);                 // the new category which will be returned.

            //variables to count through the two categories
            int i = 0;                                          // a counter for the category a
            int j = 0;                                          // a counter for the category b

            // loops until either category gets to the end.
            while (i < a.getCount() && j < b.getCount())
            {
                //if either one is putting at a null, it skips over them.  These should theoretically never be called
                if (a.getItem(i) == null)
                    i++;
                else if (b.getItem(j) == null)
                    j++;
                //if the items match, it puts it into the temp category, then increments both i and j
                else if (a.getItem(i).getId() == b.getItem(j).getId())
                {
                    if (a.getItem(i) != null)
                        temp.add(a.getItem(i));
                    i++;
                    j++;
                }
                // if the ID of category a is lower than the other, it's counter gets incremented.
                else if (a.getItem(i).getId() < b.getItem(j).getId())
                    i++;
                else if (a.getItem(i).getId() > b.getItem(j).getId())
                    j++;
            }

            if (temp.getCount() == 0)
            {
                return null;
            }
            else
            {
                a.addCrossover(b);
                a.addCrossover(temp);
                b.addCrossover(a);
                b.addCrossover(temp);
                temp.addCrossover(a);
                temp.addCrossover(b);
            }

            return temp;
        }

        public static Boolean checkCopy(List<Category> l, Category c)
        {

            if (l.Count() == 0)
            {
                return false;
            }

            foreach (Category i in l)
            {

                if (i.Equals(c))
                {
                    return true;
                }
            }
            return false;
        }

        //Call  on the list of generated categories to generate the ven diagram
        // uncomplete for now
        public static void drawVen(System.Windows.Forms.Panel p)
        {
            foreach (Category c in venCat)
            {

            }
        }


        //checks all the boxes and any one that is marked gets put into the selected category list (selCat)
        // to be called on for the actual diagram.
        public static void checkList(System.Windows.Forms.Panel p)
        {
            selCat = new List<Category>();

            // cycles through each category checkbox
            foreach (CatBox c in p.Controls)
            {
                if (c.Checked == true)
                {
                    selCat.Add(c.getCat()); //pushes it into the selected list
                }
            }
        }


        //getter for the base categories
        public static List<Category> getBaseCat()
        {
            return baseCat;
        }

        //setter for the base categories
        public static void setBaseCat(Category c)
        {
            baseCat.Add(c);
        }

        //deleter for the Selected categories
        public static void delSelCat(Category c)
        {
            selCat.Remove(c);
        }


        //getter for the Selected categories
        public static List<Category> getSelCat()
        {
            return selCat;
        }


        //setter for the base categories
        public static void setSelCat(Category c)
        {
            selCat.Add(c);
        }

        //getter for the ven diagram categories
        public static List<Category> getVenCat()
        {
            return venCat;
        }

        //getter for the base documents
        public static List<Document> getBaseDoc()
        {
            return baseDoc;
        }


        //dummy code for testing
        public static void test()
        {
            Document email1 = new Document("email 1", 1);
            Document email2 = new Document("email 2", 2);
            Document email3 = new Document("email 3", 3);
            Document email4 = new Document("email 4", 4);
            Document email5 = new Document("email 5", 5);

            Document document6 = new Document("document 6", 6);
            Document document7 = new Document("document 7", 7);
            Document document8 = new Document("document 8", 8);
            Document document9 = new Document("document 9", 9);
            Document document10 = new Document("document 10", 10);

            Document documentAll = new Document("document all", 22);

            baseDoc.Add(email1);
            baseDoc.Add(email2);
            baseDoc.Add(email3);
            baseDoc.Add(email4);
            baseDoc.Add(email5);

            Category Evens = new Category("Evens");
            Evens.add(email2);
            Evens.add(email4);
            Evens.add(document6);
            Evens.add(document8);
            Evens.add(document10);
        //    Evens.add(documentAll);

            Category Odds = new Category("Odds");
            Odds.add(email1);
            Odds.add(email3);
            Odds.add(email5);
            Odds.add(document7);
            Odds.add(document9);
        //    Odds.add(documentAll);


            Category Threes = new Category("Threes");
            Threes.add(email3);
            Threes.add(document6);
            Threes.add(document9);
        //    Threes.add(documentAll);

            Category Fours = new Category("Fours");
            Fours.add(email4);
            Fours.add(document8);
        //    Fours.add(documentAll);

            baseCat.Add(Evens);
            baseCat.Add(Odds);
            baseCat.Add(Threes);
            baseCat.Add(Fours);

        }
    }
}