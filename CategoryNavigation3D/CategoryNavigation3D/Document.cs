﻿using System;
using System.Collections.Generic;


namespace CategoryNavigation3D
{
    public class Document
    {
        private String name = null;                         //name of the document
        private int id = 0;                                 //ID number of the document
        private List<Category> cat = new List<Category>();  //list of categories it belongs  to.
        private Boolean isEmail;                            //boolean; true if email, false if document

        //constructor
        public Document(String n, int i)
        {
            name = n;
            id = i;
        }

        //constructor
        public Document(String n, int i, Boolean e)
        {
            name = n;
            id = i;
            isEmail = e;
        }

        //getters and setters
        public String getName()
        {
            return name;
        }

        public int getId()
        {
            return id;
        }

        public void addCat(Category c)
        {
            cat.Add(c);
        }

        public String toString()
        {
            return name;
        }
    }
}