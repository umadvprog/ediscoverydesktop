﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace CategoryNavigation3D
{

    class FormState
    {
        //public int cameraX { get; set; }
        //public int cameraY { get; set; }
        //public int cameraZ { get; set; }
        List<CatagorySphere> catagorySpheres = new List<CatagorySphere>();

        //values
        int step = 0;
        int baseX = 0;
        int baseY = 0;
        int baseZ = 0;
        int stepX = 100;
        int stepY = 50;
        int stepZ = 00;

        public CatagorySphere AddSphere()
        {
            CatagorySphere sphere = new CatagorySphere(baseX + step * stepX, baseY + step * stepY, baseZ + step * stepZ, "blue");
            catagorySpheres.Add(sphere);
            step++;
            return sphere;
        }

        public CatagorySphere AddSphere(String color)
        {
            //Random random = new Random();
            Console.WriteLine("Sphere created!");
            CatagorySphere sphere = new CatagorySphere(baseX + step * stepX, baseY + step * stepY, baseZ + step * stepZ, color);
            //            CatagorySphere sphere = new CatagorySphere(random.Next(0, 500), random.Next(200, 400), 0, color);
            catagorySpheres.Add(sphere);
            step++;
            return sphere;
        }

        public void DelSphere(CatagorySphere sphere)
        {
            catagorySpheres.Remove(sphere);
            step--;
        }
    }
}
