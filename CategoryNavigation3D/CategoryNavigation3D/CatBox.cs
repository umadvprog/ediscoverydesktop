﻿using System;

namespace CategoryNavigation3D
{
    //extends CheckBox to allow easier  linking to the categories
    public class CatBox : System.Windows.Forms.CheckBox
    {
        Category category = null;       //stores the relevant category.
        public CatBox()
        {
        }

        //getter for category
        public void setCat(Category c)
        {
            category = c;
        }

        //getter for category
        public Category getCat()
        {
            return category;
        }
    }
}