﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryNavigation3D
{
    public class Category
    {
        private List<Document> list = new List<Document>();                         //a list to hold all the files
        private List<String> name = new List<String>();                          //a String holding what categories this contains (the 4 words)
        private List<Category> categories = new List<Category>();                    //a string to hold the individual cateogries within a complex category
        private List<float> percentage = new List<float>();
        private List<Category> crossover = new List<Category>();                // a list of Catagories that it shares information with.


        private Boolean isNew = false;                                          // a boolean used to check if the category has been checked
        private Boolean isComplex = false;                                      // a boolean used to check if the category is a complex one
        private Boolean isChecked = false;                                      // a boolean used to check if the catbox connected is checked or not
        public CategoryNavigation3D.CatagorySphere sphere;

        //locations for the sphere
        private double x = 0;
        private double y = 0;


        //constructor
        public Category(String n)
        {
            name.Add(n);                                       // sets the name 
        }

        public Category(List<String> n)
        {
            name = n;                                       // sets the name    
        }

        public Category(List<String> n, List<String> n2)
        {
            name = n;
            name.AddRange(n2);
        }

        public Category(Category a, Category b)
        {
            foreach (String i in a.getName())
            {
                name.Add(i);
            }
            foreach (String i in b.getName())
            {
                name.Add(i);
            }
            categories.Add(a);
            categories.Add(b);
            isNew = true;
            isComplex = true;
            calcPercentage();
        }

        //checks if it contains the category from b
        public Boolean Contains(Category b)
        {
            if (categories.Contains(b))
            {
                Console.WriteLine("true");
                return true;
            }
            Console.WriteLine("false");
            return false;
        }

        public Boolean Equals(Category b)
        {
            if (categories.Contains(b))
                return true;
            if (b.getCategories().Contains(this))
                return true;
            if (categories.Count == 0 || b.getCategories().Count == 0)
                return false;
            foreach (Category i in categories)
            {
                foreach (Category j in b.getCategories())
                {
                    if (!i.Equals(j))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        //calculates the percentage of each category
        public void calcPercentage()
        {
            foreach (Category i in categories)
            {
                percentage.Add(getSize() / i.getSize());
            }
        }

        //check against another list to see if the combination already exists
        public Boolean checkNames(Category n)
        {
            List<String> name2 = n.getName();
            for (int i = 0; i < name2.Count(); i++)
            {
                if (!name.Contains(name2[i]))
                    return false;
            }
            return true;
        }

        //add a file to the array list
        public void add(Document f)
        {
            list.Add(f);
        }

        //print the full list
        public void printList()
        {
            if (list.Count > 0)
            {
                foreach (Document obj in list)
                    Console.WriteLine(obj.getName());
            }
            else
            {
                Console.WriteLine("Empty!");
            }
            Console.WriteLine();
        }

        public void addCrossover(Category c)
        {
            crossover.Add(c);
        }

        public List<Category> getCrossover()
        {
            return crossover;
        }

        public void clearCrossover()
        {
            crossover.Clear();
        }


        public void setSphere(CatagorySphere s)
        {
            sphere = s;
        }

        public CatagorySphere getSphere()
        {
            return sphere;
        }

        //collect the size of the documents in the category
        public float getSize()
        {
            return list.Count;
        }

        //getters
        public List<Category> getCategories()
        {
            return categories;
        }

        public List<String> getName()
        {
            return name;
        }

        public List<Document> getList()
        {
            return list;
        }

        public Document getItem(int i)
        {
            if (i < list.Count())
                return list[i];
            else
                return null;
        }

        public void removeItem(int i)
        {
            list.RemoveAt(i);
        }

        public int getCount()
        {
            return list.Count();
        }

        public void setIsNew(Boolean b)
        {
            isNew = b;
        }

        public Boolean getIsNew()
        {
            return isNew;
        }
        public void setIsChecked(Boolean b)
        {
            isChecked = b;
        }

        public Boolean getIsChecked()
        {
            return isChecked;
        }
        public double getX()
        {
            return x;
        }

        public void setX(double f)
        {
            x = f;
        }

        public double getY()
        {
            return y;
        }

        public void setY(double f)
        {
            y = f;
        }

        public void setIsComplex(Boolean b)
        {
            isComplex = b;
        }

        public Boolean getIsComplex()
        {
            return isComplex;
        }

        public override String ToString()
        {
            String temp = "";
            foreach (String n in name)
                temp += n + " ";
            return temp;
        }
    }
}