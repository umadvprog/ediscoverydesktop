﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using OpenTK.Platform.Windows;
using System.Data.SqlClient;

namespace CategoryNavigation3D
{
    public partial class Form1 : Form
    {
        private SQLFacade facadeConnection;
        private FormState form = new FormState();
        private List<CatagorySphere> catagorySpheres = new List<CatagorySphere>();
        int distanceFromCenter = 200;
        List<double> slicesCos = new List<double>();
        List<double> slicesSin = new List<double>();
        List<String> sphereName = new List<String>();


        //values
        int step = 0;


        public Form1()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.glControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl1_Paint);
            glControl1_Resize(this, EventArgs.Empty);   // Ensure the Viewport is set up correctly

            Application.Idle += Application_Idle;

            GL.ClearColor(Color.SkyBlue);

        }

        private void glControl1_Resize(object sender, EventArgs e)
        {
            int w = glControl1.Width;
            int h = glControl1.Height;
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, w, 0, h, -1, 100); // Bottom-left corner pixel has coordinate (0, 0)
            GL.Viewport(0, 0, w, h); // Use all of the glControl painting area
        }

        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            Render();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Application_Idle(object sender, EventArgs e)
        {
            // no guard needed -- we hooked into the event in Load handler
            while (glControl1.IsIdle)
            {
                Render();
            }
        }

        private void Render()
        {
            glControl1.MakeCurrent();
            FormRender();

            glControl1.SwapBuffers();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Master Database File|*.mdf";
            openFileDialog1.Title = "Please select a database";
            openFileDialog1.FileName = "";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    facadeConnection = new SQLFacade(openFileDialog1.FileName, false);
                    populateCheckboxes(facadeConnection.ConnectQueryCloseDB());
                    MessageBox.Show("Loaded");
                }
            }
        }

        //TODO make it handle more then documents.
        private void populateCheckboxes(SqlDataReader reader)
        {
            string currentCatName = "";
            Category currentCategory = null;
            Dictionary<int, Document> docDic = new Dictionary<int, Document>();
            try
            {
                //To edit what is thrown into the list of arrays modify here.
                //it currently accepts these tags
                //[Category ID], [Category String], [E-item Count], [E-item ID], [Document ID], [File ID], [File Name], [File Type], [Email ID]
                while (reader.Read())
                {
                    string newCatName = "" + reader["Category String"];
                    string newDocName = "" + reader["File Name"];
                    int docKey = (int) reader["E-item ID"];

                    //if cat doesn't exist. Make it
                    if (currentCatName != newCatName)
                    {
                        currentCatName = newCatName;
                        currentCategory = new Category(currentCatName);
                        DesktopApp.getBaseCat().Add(currentCategory);
                    }

                    //if doc doesn't exist. Make it
                    if (!docDic.ContainsKey(docKey))
                        docDic.Add(docKey, new Document(newDocName, docKey));

                    //add the doc to cat and cat to doc
                    currentCategory.add(docDic[docKey]);
                    docDic[docKey].addCat(currentCategory);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                facadeConnection.GetSQLConnection().Close();
            }

            foreach (var doc in docDic)
                if (doc.Value != null)
                    DesktopApp.getBaseDoc().Add(doc.Value);


            createCList();
            createDList();

            facadeConnection.GetSQLConnection().Close();
        }

        private void FormRender()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);

            lighting();
            //drawXCircularSpheres(5);
            //Drawing();

            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            foreach (CatagorySphere cata in catagorySpheres)
            {
                cata.drawCatagory(0, 0, 0);
            }
            hitDetection();

        }


        private void hitDetection()
        {
            int mouseX = PointToClient(Cursor.Position).X;
            int mouseY = PointToClient(Cursor.Position).Y;
            bool showToolTips = false;

            double drawLinesSourceX = 0, drawLinesSourceY = 0;
            CatBox drawLinesSource = new CatBox();

            bool drawLines = false;


            //TODO hook into size of list
            //handles actions when hovering over certain spots.
            //            for (int i = 0; i < slicesCos.Count; i++)

            int i = 0;
            foreach (System.Windows.Forms.Control j in this.categoryPanel.Controls)
            {
                if (j is CatBox)
                {
                    CatBox temp = j as CatBox;

                    if (temp.Checked)
                    {

                        double posX = slicesCos[i];
                        double posY = slicesSin[i];


                        if ((mouseX > slicesCos[i] - 50) && (mouseX < slicesCos[i] + 50) && (-mouseY + 590 > slicesSin[i] - 50) && (-mouseY + 590 < slicesSin[i] + 50))
                        {
                            //Set tool tip info
                            this.toolTip1.Show(sphereName[i], this, PointToClient(Cursor.Position));

                            //Show tooltips set to true
                            toolTip1.Active = true;
                            showToolTips = true;

                            var mouse = Mouse.GetState();
                            if (mouse[MouseButton.Left])
                                MessageBox.Show("This is circle " + i + "!");


                            drawLines = true;
                            drawLinesSourceX = posX;
                            drawLinesSourceY = posY;
                            drawLinesSource = temp;

                        }

                        i++;
                    }
                }
            }

            if (showToolTips == false)
                toolTip1.Active = false;

            if (drawLines)
            {

                i = 0;
                foreach (Category target in drawLinesSource.getCat().getCrossover())
                {
                    if (target.getIsChecked())
                    {
                        GL.Begin(BeginMode.Lines);
                        GL.Vertex3(drawLinesSourceX, drawLinesSourceY, 0);
                        GL.Vertex3(target.getX(), target.getY(), 0);
                        GL.End();

                        i++;
                    }
                }
            }
        }

        private void drawSpheres()
        {
            int num = 0;

            foreach (System.Windows.Forms.Control j in this.categoryPanel.Controls)
            {
                if (j is CatBox)
                {
                    CatBox temp = j as CatBox;

                    if (temp.Checked)
                    {
                        num++;
                    }
                }
            }


            slicesCos.Clear();
            slicesSin.Clear();


            catagorySpheres.Clear();

            if (num > 0)
            {
                //generate positions
                for (int i = 0; i < num; i++)
                {
                    slicesCos.Add(distanceFromCenter * Math.Cos((Math.PI / 180 * (i * 360 / num + 90))) + glControl1.Size.Width / 2);
                    slicesSin.Add(distanceFromCenter * Math.Sin((Math.PI / 180 * (i * 360 / num + 90))) + glControl1.Size.Height / 2);
                }

                hitDetection();

                //                for (int i = 0; i < num; i++)

                int j = 0;

                foreach (System.Windows.Forms.Control c in this.categoryPanel.Controls)
                {
                    if (c is CatBox)
                    {
                        CatBox temp = c as CatBox;
                        if (temp.Checked)
                        {
                            if (temp.getCat().getIsComplex())
                                catagorySpheres.Add(new CatagorySphere(slicesCos[j], slicesSin[j], 0, "red"));
                            else
                                catagorySpheres.Add(new CatagorySphere(slicesCos[j], slicesSin[j], 0, "blue"));

                            temp.getCat().setX(slicesCos[j]);
                            temp.getCat().setY(slicesSin[j]);
                            sphereName.Add(temp.getCat().ToString());
                            j++;
                        }
                    }
                }

                foreach (System.Windows.Forms.Control c in this.categoryPanel.Controls)
                {
                    if (c is CatBox)
                    {
                        CatBox temp = c as CatBox;
                        if (temp.Checked)
                        {
                        }
                    }
                }

            }


        }

        private void lighting()
        {
            SunLightDirection(1, -1, -1);
            GL.Material(MaterialFace.Front, MaterialParameter.Ambient, new float[] { 0, 0, 1, 1 });
            GL.ColorMaterial(MaterialFace.FrontAndBack, ColorMaterialParameter.Ambient);
            GL.Light(LightName.Light0, LightParameter.Ambient, new float[] { .2f, .2f, .2f, 0 });
            GL.Enable(EnableCap.Lighting);
            GL.Enable(EnableCap.Light0); ;
            GL.Enable(EnableCap.Light0);
        }

        private static void SunLightDirection(double x, double y, double z)
        {
            GL.MatrixMode(MatrixMode.Modelview);
            GL.PushMatrix();
            GL.LoadIdentity();
            GL.Rotate(-90, Vector3.UnitX);
            GL.Light(LightName.Light0, LightParameter.Position, new float[] { (float)-x, (float)-y, (float)-z, 0 });
            GL.PopMatrix();
        }

        //create a list of check boxes
        private void createCList()
        {
            this.categoryPanel.Controls.Clear();

            //creates the label
            System.Windows.Forms.Label Clabel = new System.Windows.Forms.Label();
            Clabel.Text = "Categories";
            this.categoryPanel.Controls.Add(Clabel);

            //creates the search field
            searchCat = new System.Windows.Forms.TextBox();
            searchCat.TextChanged += new EventHandler(searchCat_text);
            this.categoryPanel.Controls.Add(searchCat);

            //cycles through all the basic categorie (baseCat) and
            foreach (Category i in DesktopApp.getBaseCat())
            {
                checkBox = new CatBox();
                checkBox.setCat(i);
                checkBox.Text = i.ToString();                               // puts the name of the category in the text field
                checkBox.Click += new EventHandler(simpleCatBox_Click);
                this.categoryPanel.Controls.Add(checkBox);                                   // adds it to the panel
            }

        }

        //create a list of document check boxes
        private void createDList()
        {
            //clears existing list
            this.documentPanel.Controls.Clear();

            //creates the label
            System.Windows.Forms.Label Dlabel = new System.Windows.Forms.Label();
            Dlabel.Text = "Documents";
            this.documentPanel.Controls.Add(Dlabel);

            //creates the search field
            searchDoc = new System.Windows.Forms.TextBox();
            searchDoc.TextChanged += new EventHandler(searchDoc_text);
            this.documentPanel.Controls.Add(searchDoc);


            //cycles through all the basic categorie (baseCat) and
            foreach (Document i in DesktopApp.getBaseDoc())
            {
                checkBox = new CatBox();
                checkBox.Text = i.getName();                               // puts the name of the category in the text field
                this.documentPanel.Controls.Add(checkBox);                  // adds it to the panel
            }
        }


        //the event handler for when a category checkbox is selected.  It creates a sphere if an item is checked, and
        //destroys one if it is unchecked.
        void simpleCatBox_Click(object sender, EventArgs e)
        {
            if (sender is CatBox)
            {
                CatBox temp = sender as CatBox;
                if (!temp.getCat().getIsComplex())
                {
                    if (temp.Checked)
                    {
                        temp.getCat().setIsChecked(true);
                        DesktopApp.setSelCat(temp.getCat());        //adds it to the list
                        addCheckBox(temp);                          //adds a checkbox to the list
                        drawSpheres();
                    }
                    else
                    {
                        temp.getCat().setIsChecked(false);
                        DesktopApp.delSelCat(temp.getCat());    //removes the category from the selected list.
                        deleteCheckBox(temp);                   //removes the subsequent checkboxes
                        drawSpheres();
                    }
                }
            }
        }


        void complexCatBox_Click(object sender, EventArgs e)
        {
            if (sender is CatBox)
            {
                CatBox temp = sender as CatBox;
                if (temp.Checked)
                {
                    temp.getCat().setIsChecked(true);
                    drawSpheres();
                }
                else
                {
                    temp.getCat().setIsChecked(false);
                    drawSpheres();
                }
            }
        }

        private void addCheckBox(CatBox c)
        {

            DesktopApp.ComplexCategories(c.getCat(), 0);                    //adds any combinations based off the new categories

            foreach (Category i in DesktopApp.getVenCat())
            {
                if (i.getIsNew())
                {
                    checkBox = new CatBox();
                    checkBox.setCat(i);
                    checkBox.Text = i.ToString();                               // puts the name of the category in the text field
                    checkBox.Click += new EventHandler(complexCatBox_Click);
                    checkBox.Checked = true;
                    this.categoryPanel.Controls.Add(checkBox);                   // adds it to the panel
                    drawSpheres();
                    i.setIsNew(false);
                }
            }
        }

        private void deleteCheckBox(CatBox c)
        {
            Category a = c.getCat();

            List<CatBox> deletion = new List<CatBox>();

            foreach (System.Windows.Forms.Control i in this.categoryPanel.Controls)
            {
                if (i is CatBox)
                {
                    CatBox temp = i as CatBox;
                    Category b = temp.getCat();
                    Console.WriteLine("Compare " + a + " to " + b);
                    if (b.Contains(a))
                    {
                        Console.WriteLine("marked for deletion");
                        deletion.Add(temp);
                        step--;
                    }
                }
            }
            foreach (CatBox j in deletion)
            {
                catagorySpheres.Remove(j.getCat().getSphere());  //delete sphere
                this.categoryPanel.Controls.Remove(j);
            }
        }

        //uses the text field to hide items you aren't looking for
        void searchCat_text(object sender, EventArgs e)
        {
            foreach (System.Windows.Forms.Control i in this.categoryPanel.Controls)
            {
                if (i is CatBox)
                {
                    CatBox temp = i as CatBox;
                    if (temp.Text.Contains(searchCat.Text))
                    {
                        temp.Visible = true;
                    }
                    else
                    {
                        temp.Visible = false;
                    }
                }
            }
        }

        //uses the text field to hide items you aren't looking for
        void searchDoc_text(object sender, EventArgs e)
        {
            foreach (System.Windows.Forms.Control i in this.documentPanel.Controls)
            {
                if (i is CatBox)
                {
                    CatBox temp = i as CatBox;
                    if (temp.Text.Contains(searchDoc.Text))
                    {
                        temp.Visible = true;
                    }
                    else
                    {
                        temp.Visible = false;
                    }
                }
            }
        }


    }
}
